#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016


import json
import logging
import os
import sys
import requests
import time
import traceback
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from common.config import config_get
from agent import submit
from core.agents import getAgents, registerJob

class HPCF:

    def __init__(self):
        self.__lastPandaPoll = None
        self.__pandaJobs = 0

    def getProxy(self):
        proxyFile = config_get('proxy', 'proxy')
        return proxyFile

    def getPandaJobs(self):
        if self.__lastPandaPoll and (time.time() - self.__lastPandaPoll) < 1800:
            return self.__pandaJobs
        try:
            url = "http://bigpanda.cern.ch/status_summary/api?computingsite=NERSC_Edison"

            result = requests.get(url, verify=False, timeout=120)
            #print json.dumps(result.json(), sort_keys = False, indent = 4)
            ret = result.json()
            self.__pandaJobs = ret['data'][0]['activated']
            self.__lastPandaPoll = time.time()
            return self.__pandaJobs
        except:
            logging.debug(traceback.format_exc())
        return self.__pandaJobs

    def getRequestsConfig(self):
        """
        config = {
                  'NERSC_Edison': {
                                   "wguan-pilot-dev-HPC_merge": {
                                                                 "yodaNodes_50": {"Pilots": 4}, 
                                                                 "yodaNodes_200": {"Pilots": 4}
                                                                }
                                  }
                 }
        """
        try:
            configFile = config_get('hpcf', 'requests')
            with open(configFile) as f:
                data = json.load(f)
            return data
        except:
            logging.debug(traceback.format_exc())
        return None

    def getStartedPilots(self):
        return getAgents()

    def getNeededMorePilots(self):
        req = self.getRequestsConfig()
        # logging.debug("Requested pilots: \n%s" % json.dumps(req, sort_keys = False, indent = 4))
        started = self.getStartedPilots()
        # logging.debug("Started pilots: \n%s" % json.dumps(started, sort_keys = False, indent = 4))
        res = {}
        for site in req:
            if site in started:
                res[site] = {}
                for pilot_name in req[site]:
                    if pilot_name not in started[site]:
                        res[site][pilot_name] = req[site][pilot_name]
                    else:
                        res[site][pilot_name] = {}
                        for yodaNodes in req[site][pilot_name]:
                            res[site][pilot_name][yodaNodes] = req[site][pilot_name][yodaNodes]
                            if yodaNodes in started[site][pilot_name]:
                                res[site][pilot_name][yodaNodes]["Pilots"] = req[site][pilot_name][yodaNodes]["Pilots"] - started[site][pilot_name][yodaNodes]["Pilots"]
                                if res[site][pilot_name][yodaNodes]["Pilots"] < 0:
                                    res[site][pilot_name][yodaNodes]["Pilots"] = 0
            else:
                res[site] = req[site]
        return res

    def run(self):
        jobs = self.getPandaJobs()
        logging.debug("Activated Panda Jobs: %s" % jobs)
        if jobs < 1:
            return 0

        agentSites = []
        res = self.getNeededMorePilots()
        neededAgents = 0
        for site in res:
            for pilot_name in res[site]:
                for yodaNodes in res[site][pilot_name]:
                    neededAgents += res[site][pilot_name][yodaNodes]["Pilots"]
        if neededAgents:
            logging.debug("Needed pilots: \n%s" % json.dumps(res, sort_keys = False, indent = 4))

        for site in res:
            for pilot_name in res[site]:
                for yodaNodes in res[site][pilot_name]:
                    if res[site][pilot_name][yodaNodes]["Pilots"] and res[site][pilot_name][yodaNodes]["hostname"] not in agentSites:
                        """
                        submit.preSubmit(site=site,
                                         pilot_name=pilot_name,
                                         hostname=res[site][pilot_name][yodaNodes]["hostname"],
                                         workdir=res[site][pilot_name][yodaNodes]["workdir"],
                                         proxy_file = self.getProxy(),
                                         identify_file=res[site][pilot_name][yodaNodes]["identify_file"])
                        """
                        agentSites.append(res[site][pilot_name][yodaNodes]["hostname"])
                    prodSourceLabel = res[site][pilot_name][yodaNodes]["prodSourceLabel"] if "prodSourceLabel" in res[site][pilot_name][yodaNodes] else None
                    for i in range(res[site][pilot_name][yodaNodes]['Pilots']):
                        # site, pilot_name, hostname, workdir, yodaNodes, MVOutputDir
                        pass
                        registerJob(site=site,
                                    queue=site,
                                    pilot_name=pilot_name,
                                    hostname=res[site][pilot_name][yodaNodes]["hostname"],
                                    yodaNodes=yodaNodes.split('_')[1],
                                    yodaQueue=res[site][pilot_name][yodaNodes]["yodaQueue"] if "yodaQueue" in res[site][pilot_name][yodaNodes] else None,
                                    prodSourceLabel=res[site][pilot_name][yodaNodes]["prodSourceLabel"] if "prodSourceLabel" in res[site][pilot_name][yodaNodes] else None,
                                    MVOutputDir=None)

                        """
                        submit.submit(site=site,
                                      pilot_name=pilot_name,
                                      hostname=res[site][pilot_name][yodaNodes]["hostname"],
                                      workdir=res[site][pilot_name][yodaNodes]["workdir"],
                                      yodaNodes=yodaNodes.split('_')[1],
                                      identify_file=res[site][pilot_name][yodaNodes]["identify_file"],
                                      yodaQueue = res[site][pilot_name][yodaNodes]["yodaQueue"] if "yodaQueue" in res[site][pilot_name][yodaNodes] else None,
                                      prodSourceLabel =  res[site][pilot_name][yodaNodes]["prodSourceLabel"] if "prodSourceLabel" in res[site][pilot_name][yodaNodes] else None
                                     )
                        """


if __name__ == "__main__":
    hpcf = HPCF()
    hpcf.run()
