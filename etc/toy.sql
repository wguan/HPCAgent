
create table toy_tasks(id int not null auto_increment primary key, user char(50), name char(255), task_package char(255), executable char(255), md5 char(100), unique index(user, name))

create table toy_jobs(id int not null auto_increment primary key, task_id int, arguments char(255), hostname char(150), pid int, status char(50), output text, log text, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, started_at TIMESTAMP, finished_at TIMESTAMP, unique index(task_id, arguments))

