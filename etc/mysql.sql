
create table agents(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, site char(150), queue char(150), pilot char(255), yodaNodes int, hostname char(150), pid int, status char(50), updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)

create table jobs(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,  site char(150), queue char(150), hostname char(100), job text, agent_id int, status char(50), updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)
