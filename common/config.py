#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016

import os
import json

import ConfigParser

__CONFIG = ConfigParser.SafeConfigParser(os.environ)
__CONFIGFILES = list()

if 'HPCF_HOME' in os.environ:
    __CONFIGFILES.append('%s/etc/hpcf.cfg' % os.environ['HPCF_HOME'])

__CONFIGFILES.append('/data/HPCFactory/etc/hpcf.cfg')

__HAS_CONFIG = False
for configfile in __CONFIGFILES:
    __HAS_CONFIG = __CONFIG.read(configfile) == [configfile]
    if __HAS_CONFIG:
        break

if not __HAS_CONFIG:
    raise Exception('Could not load HPC Factory configuration file hpcf.cfg.')


def config_get(section, option):
    """Return the string value for a given option in a section"""
    return __CONFIG.get(section, option)


