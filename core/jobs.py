#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016


from core.db.DBFactory import DBFactory


db = DBFactory().createDBInstance()


def getJob(agent):
    sql = "update jobs set status='downloading', agent_id=%s where status='new' and site='%s' and queue='%s' limit 1" % (agent['id'], agent['site'], agent['queue'])
    rowNum = db.update(sql)
    if rowNum > 0:
        sql = "select id, command from jobs where status='downloading' and agent_id=%s" % agent['id']
        rows = db.get(sql)
        if rows:
            row = rows[0]
            job = {"id": row[0], "command": row[1]}
            sql = "update jobs set status='running' where id=%s" % row[0]
            db.update(sql)
            return job
    return job = {"id": None, "command": None}

def updateJob(job):
    sql = "update jobs set status='%s', finished_at=current_timestamp where id=%s" % (job['status'])
    return db.update(sql)
