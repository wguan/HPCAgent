#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016


import MySQLdb


class MySQL:
    def __init__(self, conStr):
        # mysql://rucio:rucio@localhost/rucio
        self.__conStr = conStr
        self.connect()

    def connect(self):
        conStr = self.__conStr.replace("mysql://", "")
        dbName = conStr.split("/")[1]
        dbStr = conStr.split("/")[0]
        userPass, dbHost = dbStr.split("@")
        userName, password = userPass.split(":")

        self.__db = MySQLdb.connect(host=dbHost, user=userName, passwd=password, db=dbName)
        self.__db.autocommit(True)

    def get(self, sql):
        try:
            cursor = self.__db.cursor()
            cursor.execute(sql)
            return cursor.fetchall()
        except MySQLdb.Error, e:
            self.connect()
            cursor = self.__db.cursor()
            cursor.execute(sql)
            return cursor.fetchall()

    def update(self, sql):
        try:
            cursor = self.__db.cursor()
            rowNum = cursor.execute(sql)
            return rowNum
        except MySQLdb.Error, e:
            self.connect()
            cursor = self.__db.cursor()
            rowNum = cursor.execute(sql)
            return rowNum
