#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016

"""
DB Factory.
"""

from common.config import config_get

class DBFactory:
    def __init__(self):
        pass

    def getDB(self):
        conStr = config_get('database', 'connection')
        conStr = conStr.strip()

        if conStr.startswith('mysql'):
            return "core.db.mysql.MySQL", conStr
        return None, None

    def createDBInstance(self):
        dbCon, str = self.getDB()
        components = dbCon.split('.')
        mod = __import__('.'.join(components[:-1]))
        for comp in components[1:]:
            mod = getattr(mod, comp)
        return mod(str)

