#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016

import json
from core.db.DBFactory import DBFactory


db = DBFactory().createDBInstance()


def getJob(agent_id, site, queue, host):
    if 'cori' in host:
        host = 'wguan@cori.nersc.gov'
    if 'edison' in host:
        host = 'wguan@edison.nersc.gov'
    sql = "update jobs set status='downloading', agent_id=%s where status='new' and site='%s' and queue='%s' and hostname='%s' limit 1" % (agent_id, site, queue, host)
    rowNum = db.update(sql)
    if rowNum > 0:
        sql = "select id, job from jobs where status='downloading' and agent_id=%s" % agent_id
        rows = db.get(sql)
        if rows:
            row = rows[0]
            job = {"id": row[0], "job": json.loads(row[1])}
            sql = "update jobs set status='running' where id=%s" % row[0]
            db.update(sql)
            return job
    return {"id": None, "job": None}


def registerJob(site, queue, pilot_name, hostname, yodaNodes, yodaQueue=None, prodSourceLabel=None, MVOutputDir=None):
    job = {'pilot': pilot_name,
           'yodaNodes': yodaNodes,
           'yodaQueue': yodaQueue,
           'prodSourceLabel': prodSourceLabel,
           'MVOutputDir': MVOutputDir
          }
    sql = "insert into jobs(site, queue, hostname, job, status) values('%s', '%s', '%s', '%s', 'new')" % (site, queue, hostname, json.dumps(job))
    db.update(sql)


def registerAgent(agent, ops):
    ret = {'id': None, 'status': None}
    if 'agent_id' in agent and agent['agent_id']:
        sql = "select id, status from agents where id=%s" % (agent['agent_id'])
    else:
        sql = "select id, status from agents where hostname='%s' and pid=%s" % (agent['hostname'], agent['pid'])
    rows = db.get(sql)
    sql = None
    if len(rows):
        id, status = rows[0]
        if agent['status'] == 'finished':
            ret = {'id': id, 'status': 'finished'}
        elif status == 'killing':
            ret = {'id': id, 'status': 'killing'}
        else:
            ret = {'id': id, 'status': agent['status']}
        sql = "update agents set status='%s', pilot='%s', yodaNodes=%s, updated_at=current_timestamp where id=%s" % (ret['status'], agent['pilot'], agent['yodaNodes'] if agent['yodaNodes'] else 0, id)
        db.update(sql)
        if ret['status'] in ['finished', 'killing']:
            sql = "update jobs set status='%s' where agent_id=%s" % (ret['status'], id)
            db.update(sql)
        if ops == 'jobs' and ret['status'] not in ['finished', 'killing']:
            job = getJob(id, agent['site'], agent['queue'], agent['hostname'])
            ret['job_id'] = job['id']
            ret['job'] = job['job']
        return ret
    else:
        sql = "insert into agents(site, queue, pilot, yodaNodes, hostname, pid, status) values('%s', '%s', '%s', %s, '%s', %s, '%s')" % (agent['site'], agent['queue'], agent['pilot'], agent['yodaNodes'] if agent['yodaNodes'] else 0, agent['hostname'], agent['pid'], agent['status'])
        db.update(sql)
        sql = "select id, status from agents where hostname='%s' and pid=%s" % (agent['hostname'], agent['pid'])
        rows = db.get(sql)
        id, status = rows[0]
        if ops == 'jobs':
            job = getJob(id, agent['site'], agent['queue'], agent['hostname'])
            ret = {'id': id, 'status': status, 'job_id': job['id'], 'job': job['job']}
        else:
            ret = {'id': id, 'status': status}
        return ret


def cleanAgents():
    sql = "delete from agents where TIMESTAMPDIFF(second, updated_at, CURRENT_TIMESTAMP()) > 3600 * 24 * 3"
    db.update(sql)


def getAgents():
    sql = "update agents set status='expired' where status='running' and TIMESTAMPDIFF(second, updated_at, CURRENT_TIMESTAMP()) >= 1200"
    db.update(sql)
    sql = "select site, queue, pilot, yodaNodes, status from agents where status='running' and TIMESTAMPDIFF(second, updated_at, CURRENT_TIMESTAMP()) < 1200"
    rows = db.get(sql)
    ret = {}
    for row in rows:
        site, queue, pilot, yodaNodes, status = row
        yodaNodesStr = "yodaNodes_%s" % yodaNodes
        if queue not in ret:
            ret[queue] = {}
        #if queue not in ret[site]:
        #    ret[site][queue] = {}
        if pilot not in ret[queue]:
            ret[queue][pilot] = {}
        if yodaNodesStr not in ret[queue][pilot]:
            ret[queue][pilot][yodaNodesStr] = {}
        if 'Pilots' not in ret[queue][pilot][yodaNodesStr]:
            ret[queue][pilot][yodaNodesStr] = {"Pilots": 1}
        else:
            ret[queue][pilot][yodaNodesStr]["Pilots"] += 1
    return ret
    
