#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016

from requests.status_codes import codes
from common import build_url, render_json, render_json_list, date_to_str

class Client:

    BASEURL = 'server'

    def __init__(self):
        self.server_host = config_get('server', 'hostname')

    def register(self, name, status):
        path = '/'.join([self.DIDS_BASEURL, ''])
        url = build_url(self.server_host, path=path)

        payload = {'name': name, 'status': status}

        r = self._send_request(url, type='POST', data=render_json(**data))
        if r.status_code == codes.created:
            return True
        else:
            exc_cls, exc_msg = self._get_exception(headers=r.headers, status_code=r.status_code, data=r.content)
            raise exc_cls(exc_msg)

