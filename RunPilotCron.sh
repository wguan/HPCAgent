#!/bin/bash

host=$(hostname)
if [[ $host == *"edison"* ]]; then
    echo "Edison"
    python /scratch2/scratchdirs/wguan/Edison/hpcf/agent.py --site NERSC_Edison --queue NERSC_Edison --workdir /scratch2/scratchdirs/wguan/Edison/hpcf
fi

if [[ $host == *"cori"* ]]; then
    echo "Cori"
    python /global/cscratch1/sd/wguan/pilot/hpcf/agent.py --site NERSC_Edison --queue NERSC_Edison --workdir /global/cscratch1/sd/wguan/pilot/hpcf
fi

