#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016


import json
import os
import sys
import traceback
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from web import application, ctx, data, Created, header, InternalError, OK, loadhook


from rest.controller import Controller
from common.utils import generate_http_error
from core.agents import registerAgent

urls = ('/(.*)', 'Server')

class Server(Controller):
    """ REST API for server. """

    def POST(self, ops):
        """
        Register agent.

        HTTP Success:
            200 OK

        HTTP Error:
            401 Unauthorized
        """

        header('Content-Type', 'application/json')

        try:
            json_data = json.loads(data())
        except ValueError:
            raise generate_http_error(400, 'ValueError', 'Cannot decode json parameter list')

        try:
            ret = registerAgent(json_data, ops)
            return json.dumps(ret)
        except Exception, e:
            print traceback.format_exc()
            raise InternalError(e)
        raise OK()


"""----------------------
   Web service startup
----------------------"""

app = application(urls, globals())
application = app.wsgifunc()

