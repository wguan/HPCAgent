#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016


import argparse
import commands
import getpass
import json
import logging
import os
import random
import signal
import socket
import subprocess
import sys
import time
import traceback


sys.path.append("/project/projectdirs/atlas/pilot/grid_env/external")
import requests

# Default daemon parameters.
# File mode creation mask of the daemon.
UMASK = 0


# Default maximum for the number of available file descriptors.
MAXFD = 1024

# Disable/enable stdout/stderr.
# Usually silenced, this is for developers testing & debugging
NOISY_DAEMON = False

# The standard I/O file descriptors are redirected to /dev/null by default.
if (hasattr(os, "devnull")):
   REDIRECT_TO = os.devnull
else:
   REDIRECT_TO = "/dev/null"

logging.basicConfig(filename=os.path.join(os.path.dirname(os.path.abspath(__file__)), 'agent.log'),
                    level=logging.DEBUG,
                    format='%(asctime)s\t%(process)d\t%(levelname)s\t%(message)s')


class Agent:
    def __init__(self, site, queue, workdir, MVOutputDir, pilot, yodaNodes, prodSourceLabel, yodaQueue):
        self.__hostname = socket.gethostname()
        #self.__domain = ".".join(self.__hostname.split(".")[1])
        self.__workDir = workdir
        self.__username = getpass.getuser()
        self.__site = site
        self.__queue = queue
        self.__MVOutputDir = MVOutputDir
        self.__pilotName = pilot
        self.__pilot = None
        if self.__pilotName:
            self.__pilot = "http://wguan-wisc.web.cern.ch/wguan-wisc/%s.tar.gz" % self.__pilotName
        self.__yodaNodes = yodaNodes
        self.__yodaQueue = yodaQueue
        self.__prodSourceLabel = prodSourceLabel
        self.__logFile = None
        self.__pid = os.getpid()

        self.__agent_id = None
        self.__currentDir = None
        self.__serverHost = "https://aipanda089.cern.ch:443"
        self.init_signal()

    def signal_handler(self, signum=None, frame=None):
        logging.debug("Received signal, waiting child to finish %s" % signum)

    def init_signal(self):
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGQUIT, self.signal_handler)
        signal.signal(signal.SIGSEGV, self.signal_handler)
        signal.signal(signal.SIGXCPU, self.signal_handler)
        signal.signal(signal.SIGUSR1, self.signal_handler)
        signal.signal(signal.SIGBUS, self.signal_handler)

    def preRun(self):
        command = "cp $X509_USER_PROXY /tmp/x509up_u`id -u`"
        logging.debug(command)
        status, output = commands.getstatusoutput(command)
        logging.debug(status)
        logging.debug(output)

    def createPilotScript(self):
        self.__currentDir = os.path.join(self.__workDir, self.__hostname.split(".")[0] + "/" + self.__pilotName + "/" + str(time.time()))
        if not os.path.exists(self.__currentDir):
            os.makedirs(self.__currentDir)
        os.chdir(self.__currentDir)
        logging.debug("Current Working dir: %s" % self.__currentDir)

        script = "#!/bin/bash" + "\n"

        script += "cd " + self.__currentDir + "" + "\n"
        script += "export OSG_GRID=/global/project/projectdirs/atlas/pilot/grid_env" + "\n"
        script += "export VO_ATLAS_SW_DIR=/project/projectdirs/atlas" + "\n"
        script += "source $OSG_GRID/setup.sh" + "\n"

        script += "export COPYTOOL=gfal-copy" + "\n"
        script += "export COPYTOOLIN=gfal-copy" + "\n"

        script += "export RUCIO_ACCOUNT=wguan" + "\n"

        script += "rm -f pilot.tar.gz\n"
        script += "wget " + self.__pilot + " -O pilot.tar.gz\n"
        script += "tar xzf pilot.tar.gz\n"
        script += "source /etc/profile" + "\n"
        script += "source /global/homes/" + self.__username[0] + "/" + self.__username + "/.bashrc" + "\n"
        script += "source /global/homes/" + self.__username[0] + "/" + self.__username + "/.bash_profile" + "\n"

        script += "python pilot.py -s " + self.__site + " -h " + self.__queue + "  -w https://pandaserver.cern.ch -p 25443 -d " + self.__currentDir
        if self.__prodSourceLabel:
            script += " -u " + self.__prodSourceLabel
        if self.__yodaNodes:
            script += " -N " + str(self.__yodaNodes)
        if self.__yodaQueue:
            script += " -Q " + self.__yodaQueue
        if self.__MVOutputDir:
            script += " -m " + self.__MVOutputDir
        script += " \n"

        scriptFile = os.path.join(self.__currentDir, "RunPilot.sh")
        dir = os.path.dirname(scriptFile)
        if not os.path.exists(dir):
            os.makedirs(dir)
        handle = open(scriptFile, "w")
        handle.write(script)
        handle.close()
        command = "chmod +x " + scriptFile
        status, output = commands.getstatusoutput(command)

        logFile = os.path.join(self.__currentDir, "cronlogs/pilot_cron.log")
        self.__logFile = logFile
        dir = os.path.dirname(logFile)
        if not os.path.exists(dir):
            os.makedirs(dir)
        command = scriptFile + " >" + logFile + " 2>&1"
        return command

    def checkProcess(self, pid):
        try:
            id, rc = os.waitpid(pid, os.WNOHANG)
        except OSError, e:
            logging.debug("Exception when checking process: %s" % str(e))
            if str(e).rstrip() == "[Errno 10] No child processes":
                return -1
        else:
            if id: # finished
                rc = rc % 255 # rc: 0: ok, !0: failed
                return -1
        return 0

    def getJob(self, status='running'):
        time.sleep(random.randint(1, 100))
        try:
            url = self.__serverHost + "/server/jobs"
            payload = {'site': self.__site,
                       'queue': self.__queue,
                       'pilot': self.__pilotName,
                       'yodaNodes': self.__yodaNodes,
                       'hostname': self.__hostname,
                       'pid': os.getpid(),
                       'status': status}
            result = requests.post(url, data=json.dumps(payload), verify=False, timeout=120)
            logging.debug("Get Job: %s" % result.text)
            job = result.json()
            if job and job['job']:
                self.__pilotName = job['job']['pilot']
                self.__pilot = "http://wguan-wisc.web.cern.ch/wguan-wisc/%s.tar.gz" % self.__pilotName
                self.__MVOutputDir = job['job']['MVOutputDir'] if 'MVOutputDir' in job['job'] else None
                self.__yodaNodes = job['job']['yodaNodes'] if 'yodaNodes' in job['job'] else None
                self.__yodaQueue = job['job']['yodaQueue'] if 'yodaQueue' in job['job'] else None
                self.__prodSourceLabel = job['job']['prodSourceLabel'] if 'prodSourceLabel' in job['job'] else None
                self.__agent_id = job['id']

            return job
        except:
            logging.debug(traceback.format_exc())
            return None

    def heartbeat(self, status='running'):
        try:
            url = self.__serverHost + "/server/agents"
            payload = {'site': self.__site,
                       'queue': self.__queue,
                       'pilot': self.__pilotName,
                       'yodaNodes': self.__yodaNodes,
                       'hostname': self.__hostname,
                       'pid': os.getpid(),
                       'agent_id': self.__agent_id,
                       'status': status}
            result = requests.post(url, data=json.dumps(payload), verify=False, timeout=120)
            logging.debug("Heart beat: %s" % result.text)
            return result.json()
        except:
            logging.debug(traceback.format_exc())
            return None

    def checkHeart(self, ret, p):
        try:
            if ret['status'] == 'killing':
                p.terminate()
        except:
            logging.debug(traceback.format_exc())

    def monitor(self, p):
        try:
            i = 0
            j = 0
            ret = p.poll()
            logging.debug("subprocess poll result: %s" % str(ret))
            self.heartbeat(status='running')
            while (ret is None):
                if i == 10:
                    logging.debug("subprocess poll result: %s" % str(ret))
                    heartRet = self.heartbeat(status='running')
                    self.checkHeart(heartRet, p)
                    i = 0
                if j == 60:
                    self.preRun()
                    j = 0
                time.sleep(1 * 60)
                i += 1
                j += 1
                ret = p.poll()
            logging.debug("subprocess poll result: %s" % str(ret))

            self.heartbeat(status='finished')
        except:
            logging.debug(traceback.format_exc())

    def runDaemon(self, script):
        try:
            pid = os.fork()
        except OSError, e:
            raise Exception, "%s [%d]" % (e.strerror, e.errno)

        if (pid == 0):       # The first child.
            os.setsid()
            try:
                pid = os.fork()        # Fork a second child.
            except OSError, e:
                raise Exception, "%s [%d]" % (e.strerror, e.errno)

            if (pid == 0):    # The second child.
                os.umask(UMASK)
                logging.debug("Started Daemon: Process %s" % os.getpid())
                #os.system(script)
                #output = open(self.__logFile, 'w')
                #sys.stderr = sys.stdout = output
                #out_fd = os.open(self.__logFile, os.O_WRONLY | os.O_CREAT)
                #os.dup2(out_fd, 1)
                #os.dup2(out_fd, 2)
                #command = script.split()[0]
                #args = script.split()
                #os.execv(command, args)

            else:
                os._exit(0)    # Exit parent (the first child) of the second child.
                #self.monitor(pid)
        else:
            os._exit(0)       # Exit parent of the first child.

        """
        maxfd = MAXFD
        # Iterate through and close all file descriptors.
        startDescriptor = 0
        if NOISY_DAEMON:
            startDescriptor = 3
        for fd in range(startDescriptor, maxfd):
            try:
                os.close(fd)
            except OSError:   # ERROR, fd wasn't open to begin with (ignored)
                pass
        os.open(REDIRECT_TO, os.O_RDWR)      # standard input (0)

        if not NOISY_DAEMON:
            # Duplicate standard input to standard output and standard error.
            os.dup2(0, 1)                     # standard output (1)
            os.dup2(0, 2)                     # standard error (2)
        """


        logFile = open(self.__logFile, 'a')
        logging.debug("Starting subprocess")
        p = subprocess.Popen(script, stdout=logFile, stderr=logFile, shell=True)
        logging.debug("Monitoring subprocess: %s" % p.pid)
        self.monitor(p)


    def run(self):
        self.preRun()
        if self.__pilot is None:
            logging.debug("Get Job.")
            job = self.getJob()
            if job is None or job['job'] is None:
                logging.debug("Cannot get Job. will exit.")
                return

        script = self.createPilotScript()
        self.runDaemon(script)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--site", action="store", type=str, help='site')
    parser.add_argument("--queue", action="store", type=str, help='queue')
    parser.add_argument("--workdir", action="store", type=str, help='workdir')
    parser.add_argument("--MVOutputDir", action="store", type=str, help='MVOutputDir')
    parser.add_argument("--pilot", action="store", type=str, help='pilot')
    parser.add_argument("--yodaNodes", action="store", type=int, help='yodaNodes')
    parser.add_argument("--prodSourceLabel", action="store", type=str, help='prodSourceLabel')
    parser.add_argument("--yodaQueue", action="store", type=str, help='yodaQueue')

    args = parser.parse_args()
    agent = Agent(args.site, args.queue, args.workdir, args.MVOutputDir, args.pilot, args.yodaNodes, args.prodSourceLabel, args.yodaQueue)
    #agent.heartbeat()
    #time.sleep(60)
    #agent.heartbeat()
    try:
        pass
        agent.run()
    except:
        logging.debug(traceback.format_exc())
