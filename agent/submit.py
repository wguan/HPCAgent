#!/usr/bin/env python
# Copyright European Organization for Nuclear Research (CERN)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# You may not use this file except in compliance with the License.
# You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Authors:
# - Wen Guan, <wen.guan@cern.ch>, 2016


import commands
import logging
import os

def preSubmit(site, pilot_name, hostname, workdir, identify_file, proxy_file):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    agent_path = os.path.join(current_dir, 'agent.py')
    command = "scp -i %s %s %s:%s" % (identify_file, agent_path, hostname, workdir)
    logging.debug("Send job script %s to %s: %s" % (agent_path, hostname, command))
    status, output = commands.getstatusoutput(command)
    if status:
        logging.debug("Send job script %s to %s, status: %s, output: %s" % (agent_path, hostname, status, output))

    """
    command = "scp -i %s %s %s:%s" % (identify_file, proxy_file, hostname, '~/')
    logging.debug("Send proxy %s to %s: %s" % (agent_path, hostname, command))
    status, output = commands.getstatusoutput(command)
    if status:
        logging.debug("Send prox %s to %s, status: %s, output: %s" % (agent_path, hostname, status, output))
    """


def submit(site, pilot_name, hostname, workdir, yodaNodes, identify_file, yodaQueue=None, prodSourceLabel=None, MVOutputDir=None):
    agent = os.path.join(workdir, 'agent.py')
    cmd = "python %s --site %s --queue %s --workdir %s --pilot %s --yodaNodes %s" % (agent, site, site, workdir, pilot_name, yodaNodes)
    if prodSourceLabel:
        cmd += " --prodSourceLabel %s" % prodSourceLabel
    if yodaQueue:
        cmd += " --yodaQueue %s" % yodaQueue
    if MVOutputDir:
        cmd += " --MVOutputDir %s" % MVOutputDir
    logging.debug("Submit jobs to %s: %s" % (hostname, cmd))
    command = "ssh -i %s %s %s" % (identify_file, hostname, cmd)
    logging.debug("Send jobs to %s: %s" % (hostname, command))

    status, output = commands.getstatusoutput(command)
    if status:
        logging.debug("Submit jobs to %s, status: %s, output: %s" % (hostname, status, output))


def submit2(site, queue, pilot_name, hostname, workdir, yodaNodes, identify_file, yodaQueue=None, prodSourceLabel=None, MVOutputDir=None):

    job = {'site': site,
           'queue': queue,
           'pilot': pilot_name,
           'hostname': hostname,
           'yodaNodes': yodaNodes,
           'yodaQueue': yodaQueue,
           'prodSourceLabel': prodSourceLabel,
           'MVOutputDir': MVOutputDir
          }

    if status:
        logging.debug("Submit jobs to %s, status: %s, output: %s" % (hostname, status, output))

